(function () {
    angular
        .module("calculator")
        .controller("CalculatorCtrl", CalculatorCtrl)

    function CalculatorCtrl() {
        var self = this;

        self.expression = "";

        self.addSymbol = function (symbol) {
            self.expression += symbol
        };

        self.equate = function () {
            self.expression = eval(self.expression).toString();
        };


    }

})();