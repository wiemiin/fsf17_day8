# STEPS TO CREATE CALCULATOR APP #

1. **create index.html**
2. create html, head and body tags
3. add 3 stylesheets in header `bootstrap.min.css, bootstrap-theme.min.css, font-awesome`
4. add 1 stylesheet in header of self create css file named `app.css`
5. add 3 scripts before html `jquery.min.js, angular.min.js, bootstrap.min.js`
6. add 3 scripts of self create js files before html close tag named `app.module.js, main.controller.js, calculator.controller.js`
7.  **create app.module.js **
8. name app as “calculator” and add it in `angular.module(“calculator”, [])` 
9.  **create main.controller.js **
10. use same app name in controller and name controller name as “MainCtrl”, set in `angular.module(“calculator”).controller("MainCtrl", MainCtrl)` in IFFE to manipulate in **index.html** for home page
11. create function `MainCtrl()`
12. inside function
    * set `var self = this` 
    * create object pageType for HOME and CALCULATOR
    * initialise `self.currentPage = self.pageType.HOME`
    * create function declaration, isPageOpen, to return an instructed page (HOME or CALCULATOR) by execution
    * create function declaration, open, to open an an instructed page (HOME or CALCULATOR) upon called (click) 
13.  **back in index.html **
14. create app name with directive `ng-app=“calculator”`
15. create `div class=“container”` and `ng-controller="MainCtrl as main"`
16. inside `class=“container”` include 2 html files, **home.html** and **calculator.html**
17. use `data-ng-show` to set HOME `"main.isPageOpen(main.pageType.HOME)"` by true or false, if `self.currentPage = self.pageType.HOME`, is true then show HOME
18. use ng-show (same as data-ng-show) to set CALCULATOR `"main.isPageOpen(main.pageType.CALCULATOR)"`, similiarly, if `self.currentPage = self.pageType.HOME`, is false, show CALCULATOR
19.  **create home.html **
20. create a button to proceed to the calculator app. style a calculator with font awesome
21. create a click button and manipulate the click with data-ng-click to open CALCULATOR page
22.  **create calculator.html **
23. create a class with `ng-controller="CalculatorCtrl as calculator"`
24. create a textarea for user to input number 
25. bind data from  **calculator.controller.js ** with `ng-model=“calculator.expression”`
26. create classes for operations and digits
27. each button uses `data-ng-click="calculator.addSymbol()"` for user to enter digits or symbol(% * - +) 
28. create equate button to compute mathematical inputs from digits and operations, `data-ng-click="calculator.equate()`
29. create a `back` button to go back to HOME page `data-ng-click="main.open(main.pageType.HOME)"`
30.  **create calculator.controller.js **
31. name same app name “calculator and name new controller name  "CalculatorCtrl" and set in `angular.module(“calculator”).controller("CalculatorCtrl", CalculatorCtrl)` in IFFE to manipulate in **calculator.html** for calculator page
32. create function `CalculatorCtrl()`
    * set var self = this
    * create empty `self.expression`
    * create function declaration `addSymbol` by adding user input symbol to `self.expression`
    * create function declaration `equate` by evaluating user input in `self.expression` using `eval()`
33. create `app.css` to style page and buttons 
34.  **create server/app.js **
35. require express and body-parser
36. create node port to set server location `localhost:3000`
